﻿Imports MySql.Data.MySqlClient

Public Class gestiondeempleado

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        agregarempleado.Show()
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
       
    End Sub

    Private Sub gestiondeempleado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=password"
        Dim query As String = "select * from empleado"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        Try
            'Abrimos la conexión y comprobamos que no hay error
            cnn.Open()
            cnn.Close()
        Catch ex As MySqlException
            'Si hubiese error en la conexión mostramos el texto de la descripción
            MsgBox("Error de Conexión a la Base de Datos: " & ex.Message.ToString)
            ConexionExitosa = False
        Finally
            'Eliminamos la conexion
            cnn.Dispose()
        End Try

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        editarempleado.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eliminarempleado.Show()
    End Sub
End Class