﻿Imports MySql.Data.MySqlClient
Public Class consultaempleado

    Private Sub consultaempleado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=password"
        Dim query As String = "select * from empleado"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        Try
            'Abrimos la conexión y comprobamos que no hay error
            cnn.Open()
            cnn.Close()
        Catch ex As MySqlException
            'Si hubiese error en la conexión mostramos el texto de la descripción
            MsgBox("Error de Conexión a la Base de Datos: " & ex.Message.ToString)
            ConexionExitosa = False
        Finally
            'Eliminamos la conexion
            cnn.Dispose()
        End Try

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        editarempleado.textcodigo.Text = DataGridView1.CurrentRow.Cells(0).Value
        editarempleado.textnombre.Text = DataGridView1.CurrentRow.Cells(1).Value
        editarempleado.textapellido.Text = DataGridView1.CurrentRow.Cells(2).Value
        editarempleado.ComboBox4.Text = DataGridView1.CurrentRow.Cells(3).Value
        editarempleado.textzona.Text = DataGridView1.CurrentRow.Cells(4).Value
        editarempleado.textdireccion.Text = DataGridView1.CurrentRow.Cells(5).Value
        editarempleado.texttelefono.Text = DataGridView1.CurrentRow.Cells(6).Value
        editarempleado.textcedula.Text = DataGridView1.CurrentRow.Cells(7).Value
        editarempleado.textemail.Text = DataGridView1.CurrentRow.Cells(8).Value
        editarempleado.ComboBox3.Text = DataGridView1.CurrentRow.Cells(13).Value
        editarempleado.textpuesto.Text = DataGridView1.CurrentRow.Cells(12).Value
        'editarempleado.textedad.Text = DataGridView1.CurrentRow.Cells(15).Value
        'editarempleado.ComboBox2.Text = DataGridView1.CurrentRow.Cells(14).Value
        editarempleado.fotoempleado.ImageLocation = DataGridView1.CurrentRow.Cells(11).Value
        If DataGridView1.CurrentRow.Cells(9).Value = "m" Then
            editarempleado.radiomasculino.Select()
        Else
            editarempleado.radiofemenino.Select()
        End If
        Me.Hide()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=password"
        Dim query As String = "select * from empleado where codemp like '%" + TextBox1.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=password"
        Dim query As String = "select * from empleado where nombre like '%" + TextBox2.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=password"
        Dim query As String = "select * from empleado where puesto like '%" + TextBox3.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub DataGridView1_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Button1.Select()
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Button1.Select()
    End Sub
End Class