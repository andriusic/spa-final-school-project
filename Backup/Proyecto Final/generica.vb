﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class generica
    Public Shared Function insertar(ByVal query As String, ByVal revision As String)
        Dim cnn As New MySqlConnection
        'conectar a BD
        cnn.ConnectionString = "server=localhost; user id=root; password=alpine; database=test"
        'revisar si la conexion fallo
        'Try
        '    cnn.Open()
        '    cnn.Close()
        'Catch ex As Exception
        '    MsgBox("Error de conexion")
        'End Try
        'consulta sql
        Dim adaptador As New MySqlDataAdapter
        Dim consulta = revision
        Dim comando As New MySqlCommand
        comando.Connection = cnn
        comando.CommandText = revision
        'comienza la consulta
        adaptador.SelectCommand = comando
        Dim data As MySqlDataReader
        cnn.Open()
        data = comando.ExecuteReader()
        'Verificar si el usuario existe
        If data.HasRows = 0 Then
            cnn.Close()
            cnn.Open()
            Dim registraruser As New MySqlDataAdapter
            consulta = query
            comando.Connection = cnn
            comando.CommandText = consulta
            'inicia conexion
            registraruser.SelectCommand = comando
            data = comando.ExecuteReader
            MsgBox("Registro exitoso")
        Else
            MsgBox("Usuario ya existe")
        End If
        Return query
    End Function
    Public Function consultar(ByVal query As String)

        Return query
    End Function
    Public Function actualizar(ByVal query As String, ByVal revision As String)
        Dim cnn As New MySqlConnection
        'conectar a BD
        cnn.ConnectionString = "server=localhost; user id=root; password=alpine; database=test"
        'revisar si la conexion fallo
        Try
            cnn.Open()
        Catch ex As Exception
            MsgBox("Error de conexion")
        End Try
        'consulta sql
        Dim adaptador As New MySqlDataAdapter
        Dim consulta = revision
        Dim comando As New MySqlCommand
        comando.Connection = cnn
        comando.CommandText = revision
        'comienza la consulta
        adaptador.SelectCommand = comando
        Dim data As MySqlDataReader
        cnn.Open()
        data = comando.ExecuteReader()
        'Verificar si el usuario existe
        If data.HasRows = 0 Then
            cnn.Close()
            cnn.Open()

            Dim registraruser As New MySqlDataAdapter
            consulta = query
            comando.Connection = cnn
            comando.CommandText = consulta
            'inicia conexion
            registraruser.SelectCommand = comando
            data = comando.ExecuteReader
            MsgBox("Actualizacion exitosa")
        Else
            MsgBox("Usuario ya existe")
        End If
        Return query
    End Function
    Public Function status(ByVal query As String)
        Return query
    End Function
    Public Shared Function deseasalir()
        Dim mensaje As String = "Salir"
        Dim mostrar As String = "¿Desea salir realmente?"
        Dim icono As MessageBoxIcon = MessageBoxIcon.Question
        Dim button As MessageBoxButtons = MessageBoxButtons.YesNo

        Dim opcion As MsgBoxResult = MessageBox.Show(mensaje, mostrar, button, icono)
        Select Case opcion
            Case MsgBoxResult.Yes
        End Select
        Return 0
    End Function
    Public Shared Function comboboxcargar(ByVal columna As String)
        Dim cad As String = "Database=test;Data Source=localhost;User Id=root;Password=alpine"
        Dim query As String = "select * from pais"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        Try
            'Abrimos la conexión y comprobamos que no hay error
            cnn.Open()
            cnn.Close()
        Catch ex As MySqlException
            'Si hubiese error en la conexión mostramos el texto de la descripción
            MsgBox("Error de Conexión a la Base de Datos: " & ex.Message.ToString)
            ConexionExitosa = False
        Finally
            'Eliminamos la conexion
            cnn.Dispose()
        End Try

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, columna)
        DataAdapter1.Fill(ds)

        cnn.Close()
        cnn.Dispose()
        Return ds
    End Function
End Class
