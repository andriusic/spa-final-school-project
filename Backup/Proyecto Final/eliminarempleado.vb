﻿Imports MySql.Data.MySqlClient
Public Class eliminarempleado

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=clientes;Data Source=localhost;User Id=root;Password=alpine"
        Dim query As String = "select * from empleado where codemp like '%" + TextBox1.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=clientes;Data Source=localhost;User Id=root;Password=alpine"
        Dim query As String = "select * from empleado where nombre like '%" + TextBox2.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        DataGridView1.ClearSelection()
        Dim cad As String = "Database=clientes;Data Source=localhost;User Id=root;Password=alpine"
        Dim query As String = "select * from empleado where puesto like '%" + TextBox3.Text + "%'"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub eliminarempleado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cad As String = "Database=clientes;Data Source=localhost;User Id=root;Password=alpine"
        Dim query As String = "select * from empleado"
        Dim cnn As New MySqlConnection(cad)
        Dim da As New MySqlDataAdapter(query, cnn)
        Dim ds As New DataSet
        Dim ConexionExitosa As Boolean = True

        Try
            'Abrimos la conexión y comprobamos que no hay error
            cnn.Open()
            cnn.Close()
        Catch ex As MySqlException
            'Si hubiese error en la conexión mostramos el texto de la descripción
            MsgBox("Error de Conexión a la Base de Datos: " & ex.Message.ToString)
            ConexionExitosa = False
        Finally
            'Eliminamos la conexion
            cnn.Dispose()
        End Try

        cnn.Open()
        Dim consultaSQL As MySqlCommand = New MySqlCommand(query, cnn)
        Dim DataAdapter1 As MySqlDataAdapter = New MySqlDataAdapter()
        DataAdapter1.SelectCommand = consultaSQL
        DataAdapter1.Fill(ds, "empleado")
        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "empleado"

        cnn.Close()
        cnn.Dispose()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim mensaje As String = "Esta a punto de eliminar un registro ¿Esta seguro que desea hacerlo?"
        Dim mostrar As String = "Eliminar"
        Dim icono As MessageBoxIcon = MessageBoxIcon.Question
        Dim button As MessageBoxButtons = MessageBoxButtons.YesNo

        Dim opcion As MsgBoxResult = MessageBox.Show(mensaje, mostrar, button, icono)
        Select Case opcion
            Case MsgBoxResult.Yes
                Dim cnn As New MySqlConnection
                'conectar a BD
                cnn.ConnectionString = "server=localhost; user id=root; password=alpine; database=clientes"
                'revisar si la conexion fallo
                Try
                    cnn.Open()
                Catch ex As Exception
                    MsgBox("Error conectando a la base de datos")
                End Try
                'consulta sql
                Dim adaptador As New MySqlDataAdapter
                Dim consulta As String = "update empleado set estatus='0' where codemp=" + DataGridView1.CurrentRow.Cells(0).Value + " "
                Dim comando As New MySqlCommand
                comando.Connection = cnn
                'comienza la consulta
                adaptador.SelectCommand = comando
                Dim data As MySqlDataReader
                data = comando.ExecuteReader()
        End Select
        
    End Sub
End Class