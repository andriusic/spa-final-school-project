﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class agregarempleado
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(agregarempleado))
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.textnombre = New System.Windows.Forms.TextBox
        Me.textcedula = New System.Windows.Forms.TextBox
        Me.texttelefono = New System.Windows.Forms.TextBox
        Me.textdireccion = New System.Windows.Forms.TextBox
        Me.textemail = New System.Windows.Forms.TextBox
        Me.textpuesto = New System.Windows.Forms.TextBox
        Me.radiomasculino = New System.Windows.Forms.RadioButton
        Me.radiofemenino = New System.Windows.Forms.RadioButton
        Me.textapellido = New System.Windows.Forms.TextBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.textzona = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.fotoempleado = New System.Windows.Forms.PictureBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.abrirfoto = New System.Windows.Forms.OpenFileDialog
        Me.Label1 = New System.Windows.Forms.Label
        Me.textedad = New System.Windows.Forms.TextBox
        CType(Me.fotoempleado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(207, 314)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 30)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Agregar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(324, 314)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(99, 30)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Comic Sans MS", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(159, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(264, 40)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Agregar Empleado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(10, 189)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Ciudad"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(10, 244)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Direccion"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Location = New System.Drawing.Point(420, 150)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Cedula"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Location = New System.Drawing.Point(420, 124)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Telefono"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Location = New System.Drawing.Point(10, 161)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(27, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Pais"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Location = New System.Drawing.Point(10, 127)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Apellido"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Location = New System.Drawing.Point(10, 101)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Nombre"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Location = New System.Drawing.Point(419, 174)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "E-mail"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Location = New System.Drawing.Point(419, 199)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Sexo"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Location = New System.Drawing.Point(420, 227)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Estado Civil"
        '
        'textnombre
        '
        Me.textnombre.Location = New System.Drawing.Point(78, 98)
        Me.textnombre.Name = "textnombre"
        Me.textnombre.Size = New System.Drawing.Size(100, 20)
        Me.textnombre.TabIndex = 1
        '
        'textcedula
        '
        Me.textcedula.Location = New System.Drawing.Point(491, 147)
        Me.textcedula.Name = "textcedula"
        Me.textcedula.Size = New System.Drawing.Size(100, 20)
        Me.textcedula.TabIndex = 8
        '
        'texttelefono
        '
        Me.texttelefono.Location = New System.Drawing.Point(491, 119)
        Me.texttelefono.Name = "texttelefono"
        Me.texttelefono.Size = New System.Drawing.Size(100, 20)
        Me.texttelefono.TabIndex = 7
        '
        'textdireccion
        '
        Me.textdireccion.Location = New System.Drawing.Point(78, 241)
        Me.textdireccion.Name = "textdireccion"
        Me.textdireccion.Size = New System.Drawing.Size(100, 20)
        Me.textdireccion.TabIndex = 6
        '
        'textemail
        '
        Me.textemail.Location = New System.Drawing.Point(491, 173)
        Me.textemail.Name = "textemail"
        Me.textemail.Size = New System.Drawing.Size(100, 20)
        Me.textemail.TabIndex = 9
        '
        'textpuesto
        '
        Me.textpuesto.Location = New System.Drawing.Point(491, 259)
        Me.textpuesto.Name = "textpuesto"
        Me.textpuesto.Size = New System.Drawing.Size(100, 20)
        Me.textpuesto.TabIndex = 13
        '
        'radiomasculino
        '
        Me.radiomasculino.AutoSize = True
        Me.radiomasculino.BackColor = System.Drawing.Color.Transparent
        Me.radiomasculino.Location = New System.Drawing.Point(491, 199)
        Me.radiomasculino.Name = "radiomasculino"
        Me.radiomasculino.Size = New System.Drawing.Size(73, 17)
        Me.radiomasculino.TabIndex = 10
        Me.radiomasculino.TabStop = True
        Me.radiomasculino.Text = "Masculino"
        Me.radiomasculino.UseVisualStyleBackColor = False
        '
        'radiofemenino
        '
        Me.radiofemenino.AutoSize = True
        Me.radiofemenino.BackColor = System.Drawing.Color.Transparent
        Me.radiofemenino.Location = New System.Drawing.Point(570, 199)
        Me.radiofemenino.Name = "radiofemenino"
        Me.radiofemenino.Size = New System.Drawing.Size(71, 17)
        Me.radiofemenino.TabIndex = 11
        Me.radiofemenino.TabStop = True
        Me.radiofemenino.Text = "Femenino"
        Me.radiofemenino.UseVisualStyleBackColor = False
        '
        'textapellido
        '
        Me.textapellido.Location = New System.Drawing.Point(78, 127)
        Me.textapellido.Name = "textapellido"
        Me.textapellido.Size = New System.Drawing.Size(100, 20)
        Me.textapellido.TabIndex = 2
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(78, 153)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox2.TabIndex = 3
        '
        'textzona
        '
        Me.textzona.Location = New System.Drawing.Point(78, 215)
        Me.textzona.Name = "textzona"
        Me.textzona.Size = New System.Drawing.Size(100, 20)
        Me.textzona.TabIndex = 5
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Location = New System.Drawing.Point(419, 262)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(40, 13)
        Me.Label15.TabIndex = 39
        Me.Label15.Text = "Puesto"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Location = New System.Drawing.Point(10, 214)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(32, 13)
        Me.Label16.TabIndex = 40
        Me.Label16.Text = "Zona"
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(491, 222)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox3.TabIndex = 12
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(78, 184)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox4.TabIndex = 4
        '
        'fotoempleado
        '
        Me.fotoempleado.BackgroundImage = Global.Beaty_System.My.Resources.Resources.desconocido
        Me.fotoempleado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.fotoempleado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.fotoempleado.Location = New System.Drawing.Point(254, 98)
        Me.fotoempleado.Name = "fotoempleado"
        Me.fotoempleado.Size = New System.Drawing.Size(129, 145)
        Me.fotoempleado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.fotoempleado.TabIndex = 43
        Me.fotoempleado.TabStop = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(281, 252)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 22)
        Me.Button3.TabIndex = 44
        Me.Button3.Text = "Cargar foto"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'abrirfoto
        '
        Me.abrirfoto.FileName = "Seleccione la foto"
        Me.abrirfoto.Filter = "Imagen(*.png)|"
        Me.abrirfoto.InitialDirectory = "C:\"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(12, 271)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Edad"
        '
        'textedad
        '
        Me.textedad.Location = New System.Drawing.Point(78, 268)
        Me.textedad.Name = "textedad"
        Me.textedad.Size = New System.Drawing.Size(57, 20)
        Me.textedad.TabIndex = 46
        '
        'agregarempleado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Beaty_System.My.Resources.Resources.formularissos_copia
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(646, 360)
        Me.Controls.Add(Me.textedad)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.fotoempleado)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.textzona)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.textapellido)
        Me.Controls.Add(Me.radiofemenino)
        Me.Controls.Add(Me.radiomasculino)
        Me.Controls.Add(Me.textpuesto)
        Me.Controls.Add(Me.textemail)
        Me.Controls.Add(Me.textdireccion)
        Me.Controls.Add(Me.texttelefono)
        Me.Controls.Add(Me.textcedula)
        Me.Controls.Add(Me.textnombre)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "agregarempleado"
        Me.Text = "Agregar Empleado"
        CType(Me.fotoempleado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents textnombre As System.Windows.Forms.TextBox
    Friend WithEvents textcedula As System.Windows.Forms.TextBox
    Friend WithEvents texttelefono As System.Windows.Forms.TextBox
    Friend WithEvents textdireccion As System.Windows.Forms.TextBox
    Friend WithEvents textemail As System.Windows.Forms.TextBox
    Friend WithEvents textpuesto As System.Windows.Forms.TextBox
    Friend WithEvents radiomasculino As System.Windows.Forms.RadioButton
    Friend WithEvents radiofemenino As System.Windows.Forms.RadioButton
    Friend WithEvents textapellido As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents textzona As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents fotoempleado As System.Windows.Forms.PictureBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents abrirfoto As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents textedad As System.Windows.Forms.TextBox
End Class
